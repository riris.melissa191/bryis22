from django.urls import path
from . import views

app_name = 'gifts'

urlpatterns = [
    path('', views.gifts, name='gifts'),
    path('home', views.home, name = 'home')
]
