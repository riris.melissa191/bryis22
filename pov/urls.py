from django.urls import path
from . import views

app_name = 'pov'

urlpatterns = [
    path('', views.pov, name='pov'),
    path('home', views.home, name = 'home')
]
