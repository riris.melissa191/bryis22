from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'home/home.html')

def pov(request):
    return render(request, 'pov/pov.html')

def gifts(request):
    return render(request, 'gifts/gifts.html')
